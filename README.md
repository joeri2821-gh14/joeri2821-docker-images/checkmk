# Checkmk for ARM64 #
This projects builds a checkmk-raw docker image for Linux ARM64 platform (RaspberryPi 3/4).
* Base image: ubuntu:22.04 
* Checkmk binary from: https://github.com/FloTheSysadmin/check-mk-arm/releases/
* Dockerfile and Docker-entrypoint based on: https://git.onesystems.ch/monitoring/check_mk/-/tree/master/docker

I'll try to make an updated image every 3 months.
